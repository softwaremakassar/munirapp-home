import Vue from "vue";
import "babel-polyfill";

import router from "./router.js";
import App from "./App.vue";

import "./assets/style/main.scss";

new Vue({
  render: createElement => createElement(App),
  router: router
}).$mount("#app");
